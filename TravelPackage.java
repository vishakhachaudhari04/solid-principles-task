import java.util.*;
public class TravelPackage {
    String name;
    int PassengerCapacity;

    ArrayList<Passenger> PassengerList = new ArrayList<Passenger>();
    ArrayList<Destination> DestinationinationList = new ArrayList<Destination>();

    TravelPackage(String name, int PassengerCapacity, ArrayList<Passenger> PassengerList, ArrayList<Destination> des) {
        this.name = name;
        this.PassengerCapacity = PassengerCapacity;
        ArrayList<String> Destinationination = new ArrayList<String>();
        this.DestinationinationList = des;
        this.PassengerList = PassengerList;
    }

    void printItinerary() {
        System.out.println("Travel Package Name : " + this.name);

        for (Destination individualDestination : DestinationinationList) {

            System.out.println(individualDestination);

            individualDestination.destinationDetails();
        }
    }

    void PassengerDetails() {
        System.out.println("Passenger List TP ");
        System.out.println("Travel Package Name : " + this.name);
        System.out.println("Passenger Capacity : " + this.PassengerCapacity);
        System.out.println("Number of Passengers Currently Enrolled : " + this.PassengerList.size());

        for (Passenger individualPassenger : PassengerList) {
            individualPassenger.Details();
        }

    }

    void availableActivityDetails() {
        for (Destination individualDestination : DestinationinationList) {

            for (Activity DestinationActivity : individualDestination.activities) {
                if (DestinationActivity.cap > 0) {
                    System.out.println("Activity Name : " + DestinationActivity.name);
                    System.out.println("Activity Description : " + DestinationActivity.description);
                    System.out.println("Activity Cost : " + DestinationActivity.cost);
                    System.out.println("Remaining Spaces : " + DestinationActivity.cap);
                    System.out.println("Activity Destination : " + DestinationActivity.ActivityDestination);

                }
            }

        }
    }
}
