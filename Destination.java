import java.util.*;
public class Destination {
    String name;
    ArrayList<Activity> activities = new ArrayList<Activity>();

    Destination(String name, ArrayList<Activity> activities) {
        this.name = name;
        this.activities = activities;
    }

    

    void destinationDetails() {
        System.out.println("Desatination Name : " + this.name);
        System.out.println("Activities available at " + this.name + "destination : ");
        for (Activity individualActivity : activities) {
            individualActivity.activityDetails();
        }

    }

    void addActivity(String activityName, HashMap<String, String> activityDestinationList ){
        if(activityDestinationList.containsKey(activityName)){
            System.out.println(activityName + " is already registered at " + activityDestinationList.get(activityName));
    
        }else{
            activityDestinationList.put(activityName,this.name);
            System.out.println(activityName + " is  registered successfully");
    
        }
    }

}