public class GoldPassenger extends Passenger {

    GoldPassenger(String name, int PassengerNo, int balance) {
        this.name = name;
        this.PassengerNo = PassengerNo;
        this.balance = balance;

    }

    void signUp(Activity ActivityName) {
        if (ActivityName.cap == 0) {
            System.out.println("Activity Capacity is full");
        } else if (this.balance < (ActivityName.cost - (ActivityName.cost * 0.1))) {
            System.out.println("You don't have enough money to signup for this Activity");
        } else {
            PassengerActivities.put(ActivityName, ActivityName.cost - (ActivityName.cost * 0.1));
            ActivityName.cap -= 1;
            ActivityName.cost -= (ActivityName.cost - (ActivityName.cost * 0.1));
            System.out.println("You are successfully signed up for the Activity");
        }
    }
}
