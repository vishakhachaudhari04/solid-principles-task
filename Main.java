import java.util.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<Destination> DestinationList = new ArrayList<Destination>();
        ArrayList<Activity> dombivliActivities = new ArrayList<Activity>();
        ArrayList<Activity> thaneActivities = new ArrayList<Activity>();
        ArrayList<Passenger> PassengerList = new ArrayList<Passenger>();

        HashMap<String, String> ActivityDestinationList = new HashMap<>();


        Activity a1 = new Activity("Football", "Nice Game",40,2,"Dombivli");
        Activity a2 = new Activity("Cricket", "Popular Game", 50, 2,"Thane");
        Activity a3 = new Activity("Basket Ball", "Good Game", 30,2,"Thane");
        dombivliActivities.add(a1);
        dombivliActivities.add(a2);

        thaneActivities.add(a3);

        StandardPassenger sp1 = new StandardPassenger("Vishakha", 5, 100);
        GoldPassenger gp1 = new GoldPassenger("Mina", 15, 100);
        PremiumPassenger pp1 = new PremiumPassenger("Tina", 15);
        PassengerList.add(sp1);
        PassengerList.add(gp1);
        PassengerList.add(pp1);

        sp1.signUp(a1);
        System.out.println("hello");

        sp1.PassengerDetails();
        System.out.println("done");

        Destination d1 = new Destination("Dombivli", dombivliActivities);
        Destination d2 = new Destination("Thane", thaneActivities);
       

        DestinationList.add(d1);
        DestinationList.add(d2);
        System.out.println("aaa");

        d1.addActivity(a1.name, ActivityDestinationList);
        d2.addActivity(a1.name, ActivityDestinationList);


        System.out.println("bbb");

        TravelPackage tp1 = new TravelPackage("tp1", 40, PassengerList, DestinationList);
        tp1.printItinerary();
        tp1.PassengerDetails();
        System.out.println("Available Activities : ");
        tp1.availableActivityDetails();

    }
}











