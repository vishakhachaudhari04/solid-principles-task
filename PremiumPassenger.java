
class PremiumPassenger extends Passenger {

    PremiumPassenger(String name, int PassengerNo) {
        this.name = name;
        this.PassengerNo = PassengerNo;
        this.balance = 0;

    }

    void signUp(Activity ActivityName) {
        if (ActivityName.cap == 0) {
            System.out.println("Activity Capacity is full");
        } else {
            PassengerActivities.put(ActivityName, 0.0);
            ActivityName.cap -= 1;
            System.out.println("You are successfully signed up for the Activity");
        }
    }
}