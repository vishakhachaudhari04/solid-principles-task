public class StandardPassenger extends Passenger {

    StandardPassenger(String name, int PassengerNo, int balance) {
        this.name = name;
        this.PassengerNo = PassengerNo;
        this.balance = balance;

    }

    void signUp(Activity ActivityName) {

        if (ActivityName.cap == 0) {
            System.out.println("Activity Capacity is full");
        } else if (ActivityName.cost > this.balance) {
            System.out.println("You don't have enough money to signup for this Activity");
        } else {

            double ActivityCost = ActivityName.cost;
            PassengerActivities.put(ActivityName, ActivityCost);
            ActivityName.cap -= 1;
            this.balance -= ActivityName.cost;
            System.out.println("You are successfully signed up for the Activity");
        }
    }
}
