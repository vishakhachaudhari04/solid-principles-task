import java.util.*;

public abstract class Passenger {
    String name;
    int PassengerNo;
    int balance;

    HashMap<Activity, Double> PassengerActivities = new HashMap<>();

    abstract void signUp(Activity ActivityName);

    void Details() {
        System.out.println("Passenger Name : " + this.name);
        System.out.println("Passenger Number : " + this.PassengerNo);

    }

    void PassengerDetails() {
        System.out.println("Passenger Name : " + this.name);
        System.out.println("Passenger Number : " + this.PassengerNo);
        System.out.println("Passenger Balance : " + this.balance);
        for (Map.Entry<Activity, Double> e : PassengerActivities.entrySet()) {
            System.out
                    .println("Activity Name : " + e.getKey().name + " Amount Paid for the Activity : " + e.getValue());
        }
    }
}
