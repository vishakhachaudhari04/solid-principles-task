public class Activity {
    String name;
    String description;
    int cost;
    int cap;
    String ActivityDestination;
    
    Activity(String name, String description, int cost, int cap,String ActivityDestination) {
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.cap = cap;
       
     
    }

    void activityDetails() {
        System.out.println("Activity Name : " + this.name);
        System.out.println("Activity Description : " + this.description);
        System.out.println("Activity Cost : " + this.cost);
        System.out.println("Activity Capacity : " + this.cap);

    }

}